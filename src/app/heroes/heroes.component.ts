import { Component, OnInit } from '@angular/core';
import {Hero} from '../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})

export class HeroesComponent implements OnInit {
  HERO: Hero = {
    id: 10,
    name: 'hello'
  };
  hello: string = 'aghi';
  change(){
    console.log(this.hello)
  }
  constructor() {
    console.log(this.HERO.id);

  }

  ngOnInit() {
  }

}
